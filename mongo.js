// db.users.insertOne({
//  
//     "username": "user2",
//     "password": "2nduser"
//     
// })

// db.users.insertOne({
//  
//     "username": "user3",
//     "password": "3rduser"
//     
// })

// db.users.insertMany(
//     [
//     {
//     "username": "user4",
//     "password": "4thuser"
//     },
//     {
//     "username": "user5",
//     "password": "5thuser"
//     }
//     ]
// )

// 
// db.products.insertMany(
//     [
//     {
//         "name":"product1",
//         "description":"1stproduct",
//         "price":"1000",
//     },
//     {
//         "name":"product2",
//         "description":"2ndproduct",
//         "price":"2000",
//     },
//     {
//         "name":"product3",
//         "description":"3rdproduct",
//         "price":"3000",
//     }
//     ]
// )

//Read/ Retrieve
//db.users.find()

//db.collection.find({"criteria":"value"})
// db.users.find({"username":"user5"})
// 
// db.cars.insertMany(
//     [
//     {
//     "name": "Vios",
//     "brand": "toyota",
//     "type": "sedan",
//     "price": 1500000
//     },
//     {
//     "name": "Tamaraw FX",
//     "brand": "toyota",
//     "type": "AuV",
//     "price": 750000 
//     },
//     {
//     "name": "City",
//     "brand": "honda",
//     "type": "sedan",
//     "price": 1600000
//     }
//     ])
    
// db.cars.find({"brand":"toyota"})

// db.cars.findOne({})
// db.cars.findOne({"type":"sedan"})
// db.cars.findOne({"brand":"toyota"})


//UPDATE
//db.collection.updateOne({"criteria":"value"},{$set:{"fieldtobe updated":"updatedvalue"}})
//updates the first match
//db.users.updateOne({"username":"user5"},{$set:{"username":"user51999"}})

//db.collection.updateOne({},{$set:{"fieldtobe updated":"updatedvalue"}})
//updates the first item
// db.users.updateOne({},{$set:{"username":"updatedUsername"}})

// db.users.updateOne({"username":"user51999"},{$set:{"isAdmin":true}})

db.users.updateMany({},{$set:{"isAdmin":true}})